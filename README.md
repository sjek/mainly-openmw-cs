
**as there's nothing else yet. ported from google docs for better readibilty**

# openmw-cs vs. tes-cs dialogue creation

There’s peculiar interest in specularity 


for the purposes of this document a simple mod is made with one journal entry and two topics where a sweetroll is stolen from the balcony by a cunning thief.

The thief and the sweetroll are already placed in the world and the plan is made.

```lua
Filters to remember 
	on objects or script, etc. tables
		!string("modified", "added.*")

	on instances table id is changed to
		!string("Object ID", "x.*")

	on topic infos
		!string("actor", "x.*")

	on journal infos
		!string("journal", "x.*")
```


## General remarks

1. the journal index can be used like a global variable. the set journal indexes are added to the journal
2. for diminishing loops there should be 
    1. filter at start (usually journal) and 
    1. jump over that when dialogue ends its course (quest is given). 
        - otherwise the quest dialogue can be gone through again. 
    1. as a plus placeholder dialogue can be given with a new journal index.






## Open-cs way


1. a
2. a
3. a
4. a
5. a
6. a
7. a
8. a

## Tes-cs way

1. a
2. a
3. a
4. a
5. a
6. a
7. a
8. a

